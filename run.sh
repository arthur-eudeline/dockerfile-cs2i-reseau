#!/bin/bash

# Maria DB
docker run --rm -d --name mariadbServer -p 8889:3306 -v $PWD/BDD/:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root mariadb

# Serveur Web
docker run --rm -d --name WebServer --link mariadbServer --rm -p 8888:80 -p 44:22 -v $PWD/Site/:/var/www/html/ arasaka/webserver
