# docker run -p 8080:80 -v ~/Desktop/Docker/Site:/var/www/html/

# Image de base
FROM debian

MAINTAINER Arthur Eudeline <a.eudeline@cs2i-bourgogne.com>

# Mise à jour
RUN apt-get update

# Install nano
Run apt-get install nano

# Install Apache
RUN apt-get install -y apache2 php libapache2-mod-php mariadb-server php-mysql

# Install module PHP
RUN apt-get install -y php-curl php-gd php-intl php-json php-mbstring php-xml php-zip

# Création de l'utilisateur SSH (login : dmz-ssh | pass : dmz-ssh)
RUN useradd -ms /bin/bash  dmz-ssh -p "$(openssl passwd -1 dmz-ssh)"

# Install SSH
RUN apt-get install -y openssh-server
RUN mkdir /var/run/sshd

# SSH login fix. Otherwise user is kicked off after login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

# Réglages SSH pour connecter l'utilisateur dmz-ssh au répertoire /var/www/html par défaut
# RUN printf "\nMatch User dmz-ssh\n\tChrootDirectory /var/www/html/" >> /etc/ssh/sshd_config
RUN printf "\ncd /var/www/html/" >> /home/dmz-ssh/.bashrc

# Ouverture des ports
EXPOSE 80
EXPOSE 22

VOLUME /var/www/html/

WORKDIR /var/www/html/

ADD ./ini.sh /usr/bin/ini.sh
RUN chmod 755 /usr/bin/ini.sh
CMD ["ini.sh"]
