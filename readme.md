# Guide d'utilisation

Docker file pour le serveur web Arasaka



## Sommaire 

- **1. Commandes**
- **2. Architecture du projet**
- **3. Procédure d'installation**



## 1. Commandes

| Commande      | Description                                                  |
| ------------- | ------------------------------------------------------------ |
| bash build.sh | Build de l'image Docker du serveur Apache                    |
| bash run.sh   | Lance les container du serveur Apache et MariaDB, les lient ensembles, configure les ports, associe les volumes |



## 2. Architecture du projet

- `BDD/`
  - Stockage persistant de la BDD, est directement généré par le container MariaDB
- `Site/`
  - Stockage persistant des fichiers du site
- `build.sh`
- `Dockerfile`
- `run.sh`
- `start.sh` : script de modification du Dockerfile (est chargé sur le container et ensuite exécuté pour apporter les modifications et lancer le serveur Web)



## 3. Procédure d'installation

- Se rendre dans le dossier 

- lancer la commande 

  ```shell
  bash build.sh
  ```

- lancer la commande 

  ```shell
  bash start.sh
  ```

